package br.com.andresilvaalves.bigdata.up.map;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class WordCountOrdenedMap extends Mapper<LongWritable, Text, Text, Text>{

	private final static IntWritable qtd = new IntWritable();
	private Text word = new Text();
	private Text word2 = new Text();
	
	public void map(LongWritable offset, Text lineText, Context context) throws IOException, InterruptedException {
		
		StringTokenizer itr = new StringTokenizer(lineText.toString());
		
		String tokenWord = itr.nextToken();
		if(StringUtils.isNotBlank(tokenWord)) {
			word.set(tokenWord);
			qtd.set(Integer.parseInt(itr.nextToken()));
			
			word2.set(word.toString()+" "+qtd.toString());
			
			word.set(StringUtils.leftPad(qtd.toString(), 5,"0")+ " "+word.toString());
			
			context.write(word, word2);
		}
		
	}
}
