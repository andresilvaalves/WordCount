package br.com.andresilvaalves.bigdata.up.util;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class SortByKeydesc extends WritableComparator {

	public SortByKeydesc() {
		super(Text.class, true);
	}

	@Override
	public int compare(WritableComparable o1, WritableComparable o2) {
		return o2.compareTo(o1);
	}
}
