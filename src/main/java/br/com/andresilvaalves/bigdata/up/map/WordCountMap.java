package br.com.andresilvaalves.bigdata.up.map;

import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class WordCountMap extends Mapper<LongWritable, Text, Text, IntWritable> {

	private final static IntWritable one = new IntWritable(1);

	private boolean caseSensitive = false;
	private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*\\W*");

	protected void setup(Mapper.Context context) throws IOException, InterruptedException {
		Configuration config = context.getConfiguration();
		this.caseSensitive = config.getBoolean("wordcount.case.sensitive", false);
	}

	public void map(LongWritable offset, Text lineText, Context context) throws IOException, InterruptedException {
		String line = lineText.toString();

		if (!caseSensitive) {
			line = line.toLowerCase();
		}
		
		Text currentWord = new Text();
		for (String word : WORD_BOUNDARY.split(line)) {
			word = StringUtils.trimToEmpty(word);

			if (StringUtils.isBlank(word)) {
				continue;
			}
			currentWord = new Text(word);
			context.write(currentWord, one);
		}
	}

}
