# Hadoop-WordCount

#### BIG DATA Postgraduation Course
#### Universidade Positivo - Curitiba - Brazil
#### By André Silva Alves 
##### @andresilvaalves


## commands to execute Java program

###### -- Create directories in the HDFS

```
$ hadoop fs -mkdir wordcount 
$ hadoop fs -mkdir wordcount/input
```
###### --copy files to directory

```
$ hadoop fs -put SpaceX.txt wordcount/input/
```
##### -- command to execute java program

```
$ hadoop jar WordCount.jar wordcount/input wordcount/output
```
###### -- reading results and copying to one text file

```
$ hadoop fs -cat wordcount/output/* > result.txt
$ cat result.txt
```

###### --deleting directory

```
$ hadoop fs -rm -r wordcount/output
```

## Alternatives

#### WordCount Spark


https://gitlab.com/snippets/1840940

#### WordCount Spark SQL

https://gitlab.com/snippets/1840942


